function togglerMenu() {
    let toggler = document.querySelector(".toggler");
    let navigation = document.querySelector("#navigation");
    let main = document.querySelector("#main-section");
    toggler.classList.toggle("active");
    navigation.classList.toggle("active");
    main.classList.toggle("active");
}